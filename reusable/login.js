const { Builder, By, Key, until, Alert } = require('selenium-webdriver');
require('chromedriver');

async function login (user,pswd)
{
    /* Input username dan password kemudian klik tombol Login */
    await driver.findElement(By.css("#user-name")).sendKeys(user);
    await driver.findElement(By.css("#password")).sendKeys(pswd);
    await driver.findElement(By.css("#login-button")).click();
}
async function logout ()
{
    await driver.wait(until.elementLocated(By.xpath('//button[contains(text(), "Open Menu")]'))).click();
    await driver.sleep(2000);
    await driver.findElement(By.css("#logout_sidebar_link")).click();
}

module.exports = {
  login: login,
  logout: logout,
};
