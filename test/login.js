const { Builder, By, Key, until } = require('selenium-webdriver');
var expect = require('chai').expect;
const loginModule = require('../reusable/login');
require('chromedriver')

describe('Sauce Demo', function() {
    describe('Login', function() {
        before(async function() {
            driver = await new Builder().forBrowser('chrome').build();
            await driver.get("https://www.saucedemo.com/");        
            await driver.manage().window().maximize();      
        })
        after(async function() {
            await driver.quit();
        })
        describe('User can login in sauce demo website', function(){
            it('User can login with valid account', async function() {
                let user = process.env.user;
                let pswd = process.env.psw;
                await loginModule.login(user,pswd);
                let title = await driver.wait(until.elementLocated(By.xpath("//*[@class='title']"))).getText();
                expect(title).to.equal("Products");
                await loginModule.logout();
            })
            it('Users cannot log in if they do not enter their username and password', async function() {
                await driver.findElement(By.css("#login-button")).click();
                let warn = await driver.wait(until.elementLocated(By.xpath("//h3[@data-test='error']"))).getText();
                expect(warn).to.equal("Epic sadface: Username is required");
            })
            it('User can not login with locked out account', async function() {
                let user = process.env.user_lock;
                let pswd = process.env.psw_lock;
                await loginModule.login(user,pswd);
                let warn = await driver.wait(until.elementLocated(By.xpath("//h3[@data-test='error']"))).getText();
                expect(warn).to.equal("Epic sadface: Sorry, this user has been locked out.");
            })
            it('User can not login with unregistered account', async function() {
                await driver.navigate().refresh();
                let user = process.env.user_invalid;
                let pswd = process.env.psw_invalid;
                await loginModule.login(user,pswd);
                let warn = await driver.wait(until.elementLocated(By.xpath("//h3[@data-test='error']"))).getText();
                expect(warn).to.equal("Epic sadface: Username and password do not match any user in this service");
            })
        })
    })
})
